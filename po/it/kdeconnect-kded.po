# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Vincenzo Reale <smart2128@baslug.org>, 2014, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kded\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2018-08-16 09:03+0200\n"
"PO-Revision-Date: 2015-03-28 08:19+0100\n"
"Last-Translator: Vincenzo Reale <smart2128@baslug.org>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 1.5\n"

#: kdeconnectd.cpp:50
#, kde-format
msgid "Pairing request from %1"
msgstr "Richiesta di associazione da %1"

#: kdeconnectd.cpp:51
#, kde-format
msgid "Accept"
msgstr "Accetta"

#: kdeconnectd.cpp:51
#, kde-format
msgid "Reject"
msgstr "Rifiuta"